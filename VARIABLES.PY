#Tipos de variables Python
#String
#Una cadena de caracteres
A  = "HOLA A TODOS "
B = " , :  <> { } "
c = " 74 5  7 4 5  6.6  4 75  3455646e"

#Enteros y flotantes 
D = 234
E = 56.8
F = -34.5
G = -56

#Booleanos, existen dos 
H = True 
I = False 

#Listas
#Pueden contener cualquier tipo de valor y tener muchos valores de todo tipo
J = [345, "Hola", False, -86]
#Puedes tener otras listas incluidas 
K= [ 7585, True, "Tengo hambre", [455, True , "Mike"], J ]