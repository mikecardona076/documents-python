#Condiconales en Python
#Aqui se ejecuta un bloque de código, sólo sí este cumple con su objetivo.
#En Python se usan las palabras reservadas IF, ELSE, ELIF
#Ejemplo
Edad = 21
edad_usuario = int(input("Ingresa edad: "))

if (edad_usuario >= Edad): #Bloque de la condición
    print("Puedes tomar cerveza")

elif (edad_usuario == 15): #En Python no existe el Switch, asi que podemos usar ELIF las veces que necesitemos 
    print("Muy joven")

else: #Si no se cumplen ninguna de las condiciones de arriba entonces, esta lo hará
    print("Vete a casa")